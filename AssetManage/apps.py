#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : apps.py
    *  @Author : wshu
    *  @CodeDate : 19-6-25 下午11:30
    *  @Software : PyCharm
    ***********************************
"""
from django.apps import AppConfig


class AssetmanageConfig(AppConfig):
    name = 'AssetManage'
