#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : urls.py
    *  @Author : wshu
    *  @CodeDate : 19-6-25 下午11:32
    *  @Software : PyCharm
    ***********************************
"""

from django.urls import path

from .views import (views, taskscan, assetdetails, connasset,
                    csv, port)

urlpatterns = [
    # ---资产视图--
    path('user/', views.asset_view, name='assetview'),
    path('user/list', views.asset_list, name='assetlist'),
    path('user/create', views.asset_create, name='assetcreate'),
    path('user/request/', views.asset_request, name='assetrequest'),
    path('user/delete/', views.assetdelete, name='assetdelete'),
    path('user/update/<str:asset_id>/', views.assetupdate, name='assetupdate'),
    path('user/details/<str:asset_id>/', assetdetails.assetdetailsview, name='assetdetails'),

    # ---资产扫描--
    path('user/task/', taskscan.task_action, name='assettaskaction'),

    # ---csv导出--
    path('user/csv/os/', csv.create_csv_os, name='createoscsv'),
    path('user/csv/web/', csv.create_csv_web, name='createwebcsv'),
    path('user/csv/vuln/', csv.create_csv_vuln, name='createvulncsv'),
    path('user/csv/upload/', csv.file_update, name='createuploadcsv'),

    # ---端口综合--
    path('user/port/<str:asset_id>/', assetdetails.asset_ports, name='porttable'),
    path('user/create/port/<str:asset_id>/', port.portcreate, name='portcreate'),
    path('user/update/port/<str:port_id>/', port.portupdate, name='portupdate'),
    path('user/delete/port/<str:port_id>/', port.portdelete, name='portdelete'),

    # ---资产关联--
    path('user/assetconnect/<str:asset_id>/', assetdetails.asset_asset, name='assetconnecttable'),
    path('user/create/assetconnect/<str:asset_id>/', connasset.assetconnectcreate, name='assetconnectcreate'),
    path('user/delete/assetconnect/<str:asset_id>/<str:assetconnect_id>/', connasset.assetconnectdelete,
         name='assetconnectdelete'),
]
