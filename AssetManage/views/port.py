#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : port.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/23 11:36
    *  @Software : PyCharm
    ***********************************
"""


from django.shortcuts import render,get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.utils.html import escape
from django.contrib.auth.models import User

from JQSetting.views import paging

from django.http import JsonResponse

import json as serialize

from .. import models, forms
from RBAC.models import Area

import datetime
import random, time


################################
# 添加端口
################################
@login_required
@csrf_protect
def portcreate(request,asset_id):
    user = request.user
    error =''
    if user.is_superuser:
        asset = get_object_or_404(models.Asset,asset_id=asset_id)
    else:
        asset = get_object_or_404(models.Asset,asset_user = user,asset_id=asset_id)
    if request.method == 'POST':
        form = forms.Asset_port_info(request.POST)
        if form.is_valid():
            port = form.cleaned_data['port']
            name = form.cleaned_data['name']
            product = form.cleaned_data['product']
            version = form.cleaned_data['version']
            port_info = form.cleaned_data['port_info']
            models.Port_Info.objects.get_or_create(
                port=port,
                name=name,
                product=product,
                version=version,
                port_info=port_info,
                asset=asset
                )
            error='添加成功'
        else:
            error = '请检查输入'
    else:
        form = forms.Asset_port_info()
    return render(request,'formupdate.html',{'form':form,'post_url':'portcreate','argu':asset_id,'error':error})

################################
# 添加更新
################################
@login_required
@csrf_protect
def portupdate(request,port_id):
    user = request.user
    error =''
    if user.is_superuser:
        port = get_object_or_404(models.Port_Info,id=port_id)
    else:
        port = get_object_or_404(models.Port_Info,asset__asset_user = user,id=port_id)
    if request.method == 'POST':
        form = forms.Asset_port_info(request.POST,instance =port)
        if form.is_valid():
            form.save()
            error = '端口信息已更新'
        else:
            error = '请检查输入'
    else:
        form = forms.Asset_port_info(instance =port)
    return render(request,'formupdate.html',{'form':form,'post_url':'portupdate','argu':port_id,'error':error})

################################
# 端口
################################
@login_required
def portdelete(request,port_id):
    user = request.user
    error =''
    if user.is_superuser:
        port = get_object_or_404(models.Port_Info,id=port_id)
    else:
        port = get_object_or_404(models.Port_Info,asset__asset_user = user,id=port_id)
    if port:
        port.delete()
        error = '删除成功'
    else:
        error = '非法参数'
    return JsonResponse({'error':error})