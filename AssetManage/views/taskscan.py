#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : taskscan.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/20 16:03
    *  @Software : PyCharm
    ***********************************
    Masscan + Nmap扫描
"""
import json as serialize
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required

from .. import tasks, forms, models

################################
# 端口扫描
################################
@login_required
@csrf_protect
def task_action(request):
    user = request.user
    error = ''
    asset_id_list = request.POST.get('asset_id_list')
    asset_id_list = serialize.loads(asset_id_list)
    if len(asset_id_list) == 0:
        error = '未选择符合要求资产'
    else:
        action = request.POST.get('action')
        if action == 'port':
            tasks.asset_port.delay(user.id, asset_id_list)
            error = '任务已提交'
        elif action == 'segment':
            tasks.asset_descover.delay(user.id, asset_id_list)
            error = '任务已提交'
        else:
            error = '参数错误'
    return JsonResponse(dict(error=error))
