#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : views.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:24
    *  @Software : PyCharm
    ***********************************
"""

import hashlib
from django.contrib.auth.hashers import make_password, check_password
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def paging(deploy_list, limit, offset):
    """
    分页
    :return:
    """
    paginator = Paginator(deploy_list, limit)
    try:
        deploy_list = paginator.page(offset)
    except PageNotAnInteger:
        deploy_list = paginator.page(1)
    except EmptyPage:
        deploy_list = paginator.page(paginator.num_pages)
    return deploy_list


def strtopsd(string):
    """
    :param string: 需要加密的字符串
    :return: hash + make_pass 加密生成后的秘钥
    """
    hash_res = hashlib.md5()
    hash_res.update(make_password(string).encode('utf-8'))
    urlarg = hash_res.hexdigest()
    return urlarg
