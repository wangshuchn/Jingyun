#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : admin.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:29
    *  @Software : PyCharm
    ***********************************
"""

from django.contrib import admin

from . import models

admin.site.register(models.Scanner)
admin.site.register(models.ScannerPolicies)
admin.site.register(models.Files)