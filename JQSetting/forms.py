#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : forms.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:54
    *  @Software : PyCharm
    ***********************************
"""

from . import models
from django.forms import ModelForm,widgets

class File(ModelForm):
    class Meta:
        model = models.Files
        fields = ['name', 'file_type', 'file']
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'form-control', 'placeholder': '文件名称'}),
            'file_type': widgets.Select(attrs={'class': 'form-control', 'placeholder': '文件类型'}),
            'file': widgets.FileInput(),
            }