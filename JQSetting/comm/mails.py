#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : mails.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:16
    *  @Software : PyCharm
    ***********************************
"""

from django.core.mail import EmailMultiAlternatives
from JQ.settings import DEFAULT_FROM_EMAIL, WEB_URL

url = WEB_URL

def sendemails(email, data):
    try:
        msg = EmailMultiAlternatives(data['subject'], data['text_content'], DEFAULT_FROM_EMAIL, [email])
        msg.attach_alternative(data['html_content'], "text/html")
        msg.send()
        return True
    except BaseException as err:
        print(err)
        return False

def sendregistmail(email, argu):
    """
    :param email: 对方邮箱
    :param argu: 请求参数
    :return: 成功 or 失败
    """
    data={'subject': '【可视化资产平台】激活账号-电子邮件确认',
          'text_content': '',
          'html_content': ''}

    data['text_content'] = "您的【可视化资产平台】账号激活地址如下"+ url +"/welcome/regist/"+argu +"  如无申请过该平台账号，请忽略该邮件"
    data['html_content'] = """
    <p>您好！</p>
    <p>    感谢您注册【可视化资产平台】账号，为激活您的账号，<a href='"""+ url +"/welcome/regist/"+argu +"""'>点我 </a>以完成账号激活</p>
    <p>    如点击失效，请前往访问以下地址""" + url +"/welcome/regist/"+argu + """</p>
    <p>    如果单击链接没有反应，请将链接复制到浏览器窗口中，或直接输入链接</p>
    <p>    如非本人操作，忽略该邮件</p>
    <p>    本邮件为【可视化资产平台】系统邮件，请勿回复</p>
    """
    res = sendemails(email, data)
    if res:
        return True
    else:
        return False

def send_notice_mail(email, data):
    """
    :param email: 对方邮箱
    :param argu: 请求参数
    :return: 成功 or 失败
    """
    try:
        subject = data['notice_title']
        text_content = data['notice_body']+'访问地址为：'+url+data['notice_url']
        html_content = "<p>"+data['notice_body']+"，<a href='"+url+data['notice_url']+"'><a>点我访问</a></p>"
        msg = EmailMultiAlternatives(subject,text_content, DEFAULT_FROM_EMAIL,[email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True
    except BaseException:
        return False

def sendresetpsdmail(email, argu):
    """
    :param email: 对方邮箱
    :param argu: 请求参数
    :return: 成功 or 失败
    """
    data={'subject': '【可视化资产平台】密码重置-电子邮件确认',
          'text_content': '',
          'html_content': ''}

    data['text_content'] = "您正在申请重置【可视化资产平台】密码，请前往以下地址处理："+ url +"/welcome/resetpsd/"+argu +"  如无执行重置操作，请忽略该邮件"
    data['html_content'] = """
    <p>您好！</p>
    <p>    您正在申请重置【可视化资产平台】的密码，请前往以下地址进行密码重置，<a href='"""+ url +"/welcome/resetpsd/"+argu +"""'>点我</a>以完成密码重置</p>
    <p>    如点击失效，请前往访问以下地址""" + url +"/welcome/resetpsd/" +argu + """</p>
    <p>    如果单击链接没有反应，请将链接复制到浏览器窗口中，或直接输入链接</p>
    <p>    如非本人操作，忽略该邮件</p>
    <p>    本邮件为【可视化资产平台】系统邮件，请勿回复</p>
    """
    res = sendemails(email, data)
    if res:
        return True
    else:
        return False


# if __name__ == '__main__':
#     sendregistmail('18701015792@163.com', 'agdas#ewrq1211')