#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : tool.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:22
    *  @Software : PyCharm
    ***********************************
"""

import re
import hashlib
from django.contrib.auth.hashers import make_password, check_password
# from django.core.validators import validate_email
# from django.core.exceptions import ValidationError

# 验证密码
def checkpsd(passwd):
    p = re.match(r'^(?=.*?\d)(?=.*?[a-zA-Z]).{6,}$',passwd)
    if p:
        return True
    else:
        return False


# 验证ip
def checkip(ip):
    p = re.compile('^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$')
    if p.match(ip):
        return True
    else:
        return False

'''
# def checkemail(email):
#     try:
#         validate_email(email)
#         return True
#     except ValidationError:
#         return False
'''
# 验证邮箱
def checkemail(email):
    p = re.compile('(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)')
    if p.match(email):
        return True
    else:
        return False

# 加密
def make_salt(param):
    hash_res = hashlib.md5()
    hash_res.update(make_password(param).encode('utf-8'))
    salt_value = hash_res.hexdigest()
    return salt_value