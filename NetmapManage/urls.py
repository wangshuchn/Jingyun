#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : urls.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:29
    *  @Software : PyCharm
    ***********************************
"""
from django.urls import path
from . import views

urlpatterns = [
    path('', views.Netmapview,name='netmapview'),
]