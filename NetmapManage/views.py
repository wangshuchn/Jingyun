#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : views.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:28
    *  @Software : PyCharm
    ***********************************
"""
from django.shortcuts import render


def Netmapview(request):
    return render(request, 'NetmapManage/netmaplist.html')