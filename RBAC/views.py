#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : views.py
    *  @Author : wshu
    *  @CodeDate : 18-10-2 下午1:12
    *  @Software : PyCharm
    ***********************************
"""

import datetime
import json as serialize
from . import forms, models
from django.views.decorators.csrf import csrf_protect
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, HttpResponseRedirect, get_list_or_404, get_object_or_404
from django.http import JsonResponse
from django.utils import timezone
from django.utils.html import escape
from JQSetting.comm import mails
from JQSetting.comm.tool import checkpsd, checkemail, make_salt
from JQSetting.views import paging, strtopsd
from .service.init_permission import init_permission


################################
# 仪表盘
################################
@login_required
def dashboard(request):
    return render(request, 'Dashboard.html')

################################
# 首页
################################
@login_required
def index(request):
    return render(request, 'RBAC/index.html')


################################
# 登录
################################
@csrf_protect
def login(request):
    error = ''
    if request.method == 'POST':
        form = forms.SigninForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user_get = User.objects.filter(username=username).first()
            if user_get:
                if user_get.profile.lock_time > timezone.now():
                    err = u'账号已锁定,' + str(user_get.profile.lock_time.strftime("%Y-%m-%d %H:M%")) + '后尝试重新登录'
                else:
                    user = authenticate(username=username, password=password)
                    if user:
                        user.profile.error_count = 0
                        user.save()
                        auth.login(request, user)

                        # 调用init_permission 进行权限初始化(补充说明: 因为扩展默认User表,所以初始化权限时需要传入 user.profile)
                        init_permission(request, user.profile)
                        return HttpResponseRedirect('/user/')
                    else:
                        user_get.profile.error_count += 1
                        if user_get.profile.error_count >= 10:
                            user_get.profile.lock_time = timezone.now() + datetime.timedelta(minutes=10)
                        user_get.save()
                        error = '登录失败,累积错误登录' + str(user_get.profile.error_count) + '次,10次后账号锁定'
            else:
                error = u"登录失败,用户未注册,先申请注册哦"
        else:
            error = u'请检查输入信息是否正确'
        return render(request, 'RBAC/login.html', {'form': form, 'error': error})
    else:
        if request.user.is_authenticated:
            return HttpResponseRedirect('/user/')
        else:
            form = forms.SigninForm()
    return render(request, 'RBAC/login.html', {'form':form})



################################
# 注册
################################
@csrf_protect
def regist(request, argu):
    error = ''
    if argu == 'regist':
        if request.method == "POST":
            form = forms.RegistForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                user_get = User.objects.filter(username=email)
                if user_get:
                    error = "用户已存在"
                else:
                    userregist_get = models.UserRequest.objects.filter(email=email)
                    if userregist_get.count() > 2:
                        error = '用户已多次添加'
                    else:
                        area = form.cleaned_data['area']
                        request_type = form.cleaned_data['request_type']
                        urlarg = strtopsd(email)
                        models.UserRequest.objects.get_or_create(
                            email=email,
                            urlarg=urlarg,
                            area=area,
                            request_type=request_type
                        )
                        error = '申请成功，审批通过后会向您发送邮件'
            else:
                error = '请检查输入'
        else:
            form = forms.RegistForm()
        return render(request, 'RBAC/registreq.html', {'form': form, 'error': error})
    else:
        regist_get = get_object_or_404(models.UserRequest, urlarg=argu, is_use=False)
        if request.method == 'POST':
            form = forms.Account_Reset_Form(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                firstname = form.cleaned_data['firstname']
                lastname = form.cleaned_data['lastname']
                password = form.cleaned_data['password']
                repassword = form.cleaned_data['repassword']
                username = email.split("@")[0]
                check_res = checkpsd(password)
                if check_res:
                    if regist_get.email == email:
                        if password == repassword:
                            user_create = auth.authenticate(username=username, password=password)
                            if user_create:
                                error = '用户已存在'
                            else:
                                user_create = User.objects.create_user(
                                    first_name = firstname,
                                    last_name = lastname,
                                    username=username,
                                    password=password,
                                    email=email,
                                   )
                                user_create.profile.roles.add(regist_get.request_type)
                                user_create.profile.area=regist_get.area
                                user_create.save()
                                regist_get.is_use=True
                                regist_get.save()
                                return HttpResponseRedirect('/welcome/')
                        else:
                            error = '两次密码不一致'
                    else:
                        error = '请核实邮箱是否为注册时所填写邮箱'
                else:
                    error = '恶意注册是不对滴'
            else:
                error = '密码必须6位以上且包含字母、数字'
        else:
            form = forms.Account_Reset_Form()
        return render(request, 'RBAC/regist.html', {'form': form, 'error':error})

################################
# 申请重置密码/确认重置
################################
@csrf_protect
def resetpasswd(request, argu='resetpsd'):
    error = ''
    if argu == 'resetpsd':
        if request.method == 'POST':
            form = forms.ResetpsdRequestForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                if checkemail(email):
                    user = get_object_or_404(User, email=email)
                    if user:
                        urlarg = make_salt(email)
                        models.UserResetpsd.objects.get_or_create(
                            email=email,
                            urlarg=urlarg
                        )
                        res = mails.sendresetpsdmail(email, urlarg)
                        if res:
                            error = '申请已发送，请注意检查邮箱邮件通知'
                        else:
                            error = '重置邮件发送失败，请重试'
                    else:
                        error = '重置邮件发送失败，请重试'
                else:
                    error = '请输入有效的电子邮箱'
            else:
                error = '请检查输入信息'
        else:
            form = forms.ResetpsdRequestForm()
        return render(request, 'RBAC/resetpsdq.html', {'form': form, 'error': error})
    else:
        resetpsd = get_object_or_404(models.UserResetpsd, urlarg=argu)
        if resetpsd:
            email_get = resetpsd.email
            if request.method == 'POST':
                form = forms.ResetpsdForm(request.POST)
                if form.is_valid():
                    email = form.cleaned_data['email']
                    password = form.cleaned_data['password']
                    repassword = form.cleaned_data['repassword']
                    if checkpsd(password):
                        if password == repassword:
                            if email_get == email:
                                user = get_object_or_404(User, email=email)
                                if user:
                                    user.set_password(password)
                                    user.save()
                                    resetpsd.delete()
                                    return HttpResponseRedirect('/welcome/')

                                else:
                                    error = '用户信息有误'
                            else:
                                error = '用户邮箱不匹配'
                        else:
                            error = '两次密码不一致'
                    else:
                        error = '密码必须6位以上且包含字母、数字'
                else:
                    error = '请检查输入'
            else:
                form = forms.ResetpsdForm()
            return render(request, 'RBAC/resetpsd.html', {'form': form, 'error': error, 'title': '重置'})

################################
# 登出
################################
@login_required
def logout(request):
    auth.logout(request)
    request.session.clear()
    return HttpResponseRedirect('/welcome/')



################################
## 修改密码
################################
@login_required
@csrf_protect
def changepwd(request):
    error = ''
    if request.method == 'POST':
        form = forms.ChangePwdForm(request.POST)
        if form.is_valid():
            old_password = form.cleaned_data['old_password']
            new_password = form.cleaned_data['new_password']
            re_new_password = form.cleaned_data['re_new_password']
            username = request.user.username
            if checkpsd(new_password):
                if new_password and new_password == re_new_password:
                    if old_password:
                        user = auth.authenticate(username=username, password=old_password)
                        if user:
                            user.set_password(new_password)
                            user.save()
                            auth.logout(request)
                            error = '修改成功'
                        else:
                            error = '账号信息错误'
                    else:
                        error = '请检查原密码'
                else:
                    error = '两次输入的密码不一致,请自己确认检查'
            else:
                error = '密码必须在6位以上,并且包含数字, 字母!'
        else:
            error = '请检查输入'
        return render(request, 'formEdit.html', {'form': form, 'post_url': 'changepwd', 'error':error})
    else:
        form = forms.ChangePwdForm()
        return render(request, 'formEdit.html', {'form': form, 'post_url': 'changepwd'})

################################
# 用户列表/根据属地检索
################################
@login_required
@csrf_protect
def userlist(request):
    user = request.user
    error = ''
    if user.is_superuser:
        area = models.Area.objects.filter(parent__isnull=True)
        city = models.Area.objects.filter(parent__isnull=False)
        return render(request, 'RBAC/userlist.html', {'area': area, 'city': city})
    else:
        error = '权限错误'
    return render(request, 'error.html', {'error': error})

################################
# 审批
################################
@login_required
@csrf_protect
def userregistaction(request):
    user = request.user
    error = ''
    if user.is_superuser:
        regist_id = request.POST.get('request_id')
        action = request.POST.get('action')
        userregist = get_object_or_404(models.UserRequest, id=regist_id)    # 模型层和视图层低耦合
        if userregist.is_check:
            error = '请勿重复审批'
        else:
            if action == 'access':
                userregist.is_check = True
                userregist.status = '1'
                res = mails.sendregistmail(userregist.email, userregist.urlarg)
                if res:
                    error = '添加成功，已向该员工发送邮件'
                else:
                    error = '添加成功，邮件发送失败，请重试'
                userregist.save()
            else:
                if action == 'deny':
                    userregist.is_check = True
                    userregist.status = '2'
                    userregist.is_use = True
                    userregist.save()
                    error = '已审批'
                else:
                    error = '未指定操作'
    else:
        error = '权限错误'
    return JsonResponse({'error': error})

################################
# 注册审批列表综合展示
################################
REAUEST_STATUS={
    '0': '待审批',
    '1': '审批通过',
    '2': '审批拒绝',
    }

@login_required
@csrf_protect
def userregisttable(request):
    user = request.user
    resultdict = {}
    error = ''
    page = request.POST.get('page')
    rows = request.POST.get('limit')

    email = request.POST.get('email')
    if not email:
        email = ''
    status = request.POST.get('status')
    if not status:
        status = ''
    is_use = request.POST.get('is_use')
    if not is_use:
        is_use = ['True', 'False']
    else:
        is_use = [is_use]
    is_check = request.POST.get('is_check')
    if not is_check:
        is_check = ['True', 'False']
    else:
        is_check = [is_check]

    if user.is_superuser:
        userrequest_list = models.UserRequest.objects.filter(email__icontains=email, status__icontains=status,
                                                             is_use__in=is_use, is_check__in=is_check).order_by(
            'is_check', 'is_use', '-updatetime')
        total = userrequest_list.count()
        userrequest_list = paging(userrequest_list, rows, page)
        data = []
        for userrequest in userrequest_list:
            dic = dict()
            dic['request_id'] = escape(userrequest.id)
            dic['email'] = escape(userrequest.email)
            if userrequest.is_check:
                dic['is_check'] = escape('已审批')
                dic['starttime'] = escape(userrequest.starttime)
                if userrequest.action_user:
                    dic['action_user'] = escape(userrequest.action_user.username)
                dic['updatetime'] = escape(userrequest.updatetime)
            else:
                dic['is_check'] = escape('待审批')
            if userrequest.is_use:
                dic['is_use'] = escape('已使用')
            else:
                dic['is_use'] = escape('待使用')
            dic['request_type'] = escape(userrequest.request_type.title)
            dic['status'] = escape(REAUEST_STATUS[userrequest.status])
            data.append(dic)
        resultdict['code'] = 0
        resultdict['msg'] = "用户申请列表"
        resultdict['count'] = total
        resultdict['data'] = data
        return JsonResponse(resultdict)
    else:
        error = '权限错误'
    return render(request, 'error.html', {'error': error})

################################
# 注册列表
################################
@login_required
def userregistlist(request):
    user = request.user
    error = ''

    if user.is_superuser:
        area = models.Area.objects.filter(parent__isnull=True)
        return render(request, 'RBAC/userregistlist.html', {'area': area})
    else:
        error = '权限错误'
    return render(request, 'error.html', {'error': error})


################################
# 用户列表综合展示
################################
@login_required
@csrf_protect
def userlisttable(request):
    user = request.user
    error = ''
    resultdt = {}

    page = request.POST.get('page')
    rows = request.POST.get('limit')
    email = request.POST.get('email')

    if not email:
        email = ''

    area = request.POST.get('area')
    if not area:
        area_get = models.Area.objects.filter(parent__isnull=True)
    else:
        area_get =models.Area.objects.filter(id=area)


    is_active = request.POST.get('is_active')
    if not is_active:
        is_active = ['True', 'False']
    else:
        is_active = [is_active]

    if user.is_superuser:
        user_list = User.objects.filter(email__icontains=email,
                                        profile__area__in=list(area_get.values_list('id', flat=True)),      # 优化查询，__in使用定义好的数组做查询优化
                                        is_active__in=is_active).order_by('-is_superuser', '-date_joined')
        total = user_list.count()
        user_list = paging(user_list, rows, page)
        data = list()
        for user_item in user_list:
            dit = dict()
            dit['name'] = user_item.first_name + user_item.last_name
            dit['email'] = user_item.email
            dit['date'] = user_item.date_joined
            if user_item.profile.area:
                dit['area'] = user.profile.area.name
            else:
                dit['area'] = '未知'
            dit['title'] = user_item.profile.title
            if user_item.is_active:
                dit['status'] = '启用'
            else:
                dit['status'] = '禁用'

            dit['lastlogin'] = user_item.last_login
            role = user_item.profile.roles.all()
            roles = list()
            for i in role:
                roles.append(i.title)
            dit['role'] = roles
            data.append(dit)
        resultdt['code'] = 0
        resultdt['msg'] = '用户列表'
        resultdt['count'] = total
        resultdt['data'] = data
        return JsonResponse(resultdt)
    else:
        error = '权限错误'
    return render(request, 'error.html', {'error': error})

################################
# 添加用户
################################
@login_required
@csrf_protect
def user_add(request):
    user = request.user
    error = ''
    if user.is_superuser:
        if request.method == 'POST':
            form = forms.RegistForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                user_get = User.objects.filter(username=email)
                if user_get:
                    error = '用户已存在'
                else:
                    userregist_get = models.UserRequest.objects.filter(email=email)
                    if userregist_get.count() > 2:
                        error = '用户已多次添加'
                    else:
                        area = form.cleaned_data['area']
                        request_type = form.cleaned_data['request_type']
                        urlarg = strtopsd(email)
                        models.UserRequest.objects.get_or_create(
                            email=email,
                            urlarg=urlarg,
                            area=area,
                            request_type=request_type,
                            is_check=True,
                            status='1',
                            action_user=user
                        )
                        res = mails.sendregistmail(email, urlarg)
                        if res:
                            error = '添加成功，已向该员工发送邮件'
                        else:
                            error = '添加成功，邮件发送失败，请重试'
            else:
                error = '请检查输入'
        else:
            form = forms.RegistForm()
    else:
        error = '请检查权限是否正确'
    return render(request, 'formEdit.html', {'form': form, 'post_url':'useradd', 'error': error})

################################
## 用户资料
################################

@login_required
def userinfo(request):
    return render(request, 'RBAC/userInfo.html')

################################
## 修改资料
################################
@login_required
@csrf_protect
def changeuserinfo(request):
    user = request.user
    error = ''
    if request.method == 'POST':
        form = forms.UserInfoForm(request.POST, instance=user.profile)
        if form.is_valid():
            if 'parent_email' in form.changed_data:
                parent_email = form.cleaned_data['parent_email']
                parent_user = User.objects.filter(email=parent_email).first()
                if parent_user:
                    user.profile.parent = parent_user
                    user.save()
            form.save()
            error = '修改成功'
        else:

            error = '修改失败'
        return render(request, 'formEdit.html', {'form': form, 'post_url': 'changeuserinfo', 'error':error})
    else:
        form = forms.UserInfoForm(instance=user.profile)
        return render(request, 'formEdit.html', {'form':form, 'post_url': 'changeuserinfo'})

################################
# 禁用该用户注册权限 by/2020.2.20
##############################
@login_required
@csrf_protect
def user_request_cancel(request):
    user = request.user
    error = ''
    if user.is_superuser:
        regist_id_list = request.POST.get('regist_id_list')
        regist_id_list = serialize.loads(regist_id_list)    # 序列化regist_id_list
        action = request.POST.get('action')
        for regist_id in regist_id_list:
            userregist = get_object_or_404(models.UserRequest, id=regist_id)
            userregist.status = '2'
            userregist.is_check = True
            userregist.is_use = True
            userregist.save()
        error = '已禁用'
    else:
        error = '权限错误'
    return JsonResponse({'error':error})


################################
## 启用/禁用账号
###############################
@login_required
@csrf_protect
def user_disactivate(request):
    user = request.user
    error = ''
    if user.is_superuser:
        user_list = request.POST.get('user_list')
        user_list = serialize.loads(user_list)    # 序列化user_list
        action = request.POST.get('action')
        for user_mail in user_list:
            user_get = get_object_or_404(User, email=user_mail)
            if action == 'stop':
                user_get.is_active = False
                user_get.save()
                error = '已禁用'
            elif action == 'start':
                user_get.is_active = True
                user_get.save()
                error = '已启用'
            else:
                error = '操作失败'
    else:
        error = '权限错误'
    return JsonResponse({'error': error})