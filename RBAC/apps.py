#!/usr/bin/env python
# -*-coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : apps.py
    *  @Author : wshu
    *  @CodeDate : 18-9-27 下午2:30
    *  @Software : PyCharm
    ***********************************
"""

from django.apps import AppConfig


class RbacConfig(AppConfig):
    name = 'RBAC'
