#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : admin.py
    *  @Author : wshu
    *  @CodeDate : 18-10-2 下午4:26
    *  @Software : PyCharm
    ***********************************
"""
from django.contrib import admin

from . import models

admin.site.register(models.Menu)
admin.site.register(models.Permission)
admin.site.register(models.Role)
admin.site.register(models.Area)
admin.site.register(models.Profile)
admin.site.register(models.UserRequest)
