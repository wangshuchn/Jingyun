#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : urls.py
    *  @Author : wshu
    *  @CodeDate : 2020/2/16 11:40
    *  @Software : PyCharm
    ***********************************
"""
from django.urls import path

from . import views

urlpatterns = [
    path('user/', views.article_view, name='articleview'),
    path('user/list/', views.article_list, name='articlelist'),
    path('user/details/', views.article_details, name='articledetails'),


    path('user/create/', views.article_create, name='articlecreate'),
]
