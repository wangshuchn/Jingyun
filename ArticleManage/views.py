#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : views.py
    *  @Author : wshu
    *  @CodeDate : 2019/6/12 11:42
    *  @Software : PyCharm
    ***********************************
"""
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.utils.dateformat import time_format
from django.utils.html import escape

from . import models, forms
from JQSetting.views import paging

import time
import uuid as sk

################################
# 文章视图
################################
@login_required
def article_view(request):
    article_type = models.ArticleType.objects.filter(parent__isnull=False)
    # print(article_type)
    return render(request, 'ArticleManage/articlelist.html', locals())


################################
# 列表
################################
@login_required
@csrf_protect
def article_list(request):
    return JsonResponse('ok')




################################
# 详情
################################
@csrf_exempt
@login_required
def article_details(request):
    return render(request, 'ArticleManage/articledetails.html')


################################
# 创建
################################
@login_required
@csrf_protect
def article_create(request):
    user = request.user
    error = ''

    if user.is_superuser:
        if request.method == 'POST':
            form = forms.ArticleEditForm(request.POST, request.FILES)
            if form.is_valid():
                try:
                    num_id = models.Article.objects.latest('id').id
                except:
                    num_id = 0
                num_id += 1
                article_type = form.cleaned_data['article_type']
                type_id = article_type.id
                article_id = str(type_id) + time.strftime('%Y%m%d', time.localtime(time.time())) + str(num_id)
                article_name = form.cleaned_data['article_name']
                article_type = form.cleaned_data['article_type']
                article_body = form.cleaned_data['article_body']
                file = form.cleaned_data['file']
                if file:
                    file_suffix = file.name.split('.')[-1]
                    file_name = str(sk.uuid1()) + '.' + file_suffix
                    file.name = file_name
                models.Article.objects.get_or_create(
                    article_id=article_id,
                    article_name=article_name,
                    article_type=article_type,
                    article_body=article_body,
                    article_user=user,
                    file=file,
                )
                error = '创建成功'
        else:
            form = forms.ArticleEditForm()
        return render(request, 'formEdit.html', dict(form=form, post_url='articlecreate', error=error))
    else:
        error = '权限错误'
    return render(request, 'error.html', dict(error=error))