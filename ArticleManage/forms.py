#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : forms.py
    *  @Author : wshu
    *  @CodeDate : 2019/6/11 10:23
    *  @Software : PyCharm
    ***********************************
"""
from django.forms import ModelForm
from . import models
from django.forms import widgets

class ArticleEditForm(ModelForm):
    class Meta:
        model = models.Article
        fields = ['article_name', 'article_type', 'article_body', 'file']
        widgets = {
            'article_name': widgets.TextInput(attrs={'class': 'form-control', 'placeholder': '文章名称'}),
            'article_type': widgets.Select(attrs={'class': 'form-control'}),
            'article_body': widgets.Textarea(attrs={'class': 'form-control', 'placeholder': '知识库内容'}),
            'file': widgets.FileInput(),
        }