#!/usr/bin/env python
# -*- coding:utf-8 -*-
from __future__ import absolute_import, unicode_literals

__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : celery.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/17 11:01
    *  @Software : PyCharm
    ***********************************
"""

from django.conf import settings

import os
from celery import Celery, platforms

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'JQ.settings')

basiapp = Celery('JQ')
# 设置Django为Celery的配置源
basiapp.config_from_object('django.conf:settings', namespace='CELERY')
# Load task modules from all registered Django app configs.
basiapp.autodiscover_tasks(lambda : settings.INSTALLED_APPS)
platforms.C_FORCE_ROOT = True

@basiapp.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))






