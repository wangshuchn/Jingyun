#!/usr/bin/env python
# -*-coding:utf-8 -*-
# @Time : 2019/7/15 18:13
# @Auther : Wshu
from __future__ import absolute_import

from .celery import basiapp as celery_app
import pymysql
# 加载celery应用
__all__ = ('celery_app',)
# 加载MySQL驱动
pymysql.install_as_MySQLdb()