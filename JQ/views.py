#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : views.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 23:06
    *  @Software : PyCharm
    ***********************************
"""

from django.shortcuts import render


def page_not_found(request, exception):
    return render(request, '404.html')


def page_error(request):
    return render(request, '500.html')


def permission_denied(request, exception):
    return render(request, '403.html')