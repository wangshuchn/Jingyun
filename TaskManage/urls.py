#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : urls.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:31
    *  @Software : PyCharm
    ***********************************
"""
from django.urls import path
from .views import views, goscan


urlpatterns = [
    # --user--
    path('user/', views.task_view, name='taskview'),
    path('user/list', views.task_list, name='tasklist'),
    path('user/addtask/', goscan.ScanAll, name='scantask'),
    path('user/details/<str:task_id>/', views.taskdetails, name='taskdetails'),

    path('user/scantaskchoice/<str:action>/', goscan.scan_task, name='taskscanchoice'),

    path('user/scan/action/<str:task_id>/<str:action>/', views.task_action, name='taskaction'),
    path('user/task/action/<str:task_id>/<str:action>/', views.taskrequestaction, name='taskrequestaction'),

    # --manage--
    path('manage/sync/', views.task_sync, name='tasksync'),

    path('request/', views.taskReqview, name='taskreqview'),
    path('request/list/', views.taskReqlist, name='taskreqlist'),

]