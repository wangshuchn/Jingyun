#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : admin.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:31
    *  @Software : PyCharm
    ***********************************
"""
from django.contrib import admin

from . import models

admin.site.register(models.Task)