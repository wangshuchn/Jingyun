#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : views.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/14 12:30
    *  @Software : PyCharm
    ***********************************
"""
from django.shortcuts import render, get_object_or_404
from django.http.response import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.utils.html import escape
from JQSetting.views import paging
from .goscan import sys_action, web_action

from .. import models, forms
import time



################################
# 任务管理
################################
def task_view(request):
    return render(request, 'TaskManage/tasklist.html')

################################
# 任务下发
################################
def taskReqview(request):
    return render(request, 'TaskManage/taskrequest.html')

################################
# 任务下发列表
################################
@login_required
@csrf_protect
def taskReqlist(request):
    user = request.user
    resultdict = {}
    page = request.POST.get('page')
    rows = request.POST.get('limit')

    if user.is_superuser:
        task_list = models.Task.objects.filter(task_status=0).order_by('task_starttime')
        total = task_list.count()
        task_list = paging(task_list, rows, page)
        data = []
        for item in task_list:
            dic = {}
            dic['task_id'] = escape(item.task_id)
            dic['task_name'] = escape(item.task_name)
            dic['task_type'] = escape(item.task_type)
            dic['task_target'] = escape(item.task_target)
            dic['task_starttime'] = escape(item.task_starttime)
            dic['task_scanner'] = escape(item.task_scanner.scanner_name)
            dic['task_user'] = escape(item.task_user.email)
            data.append(dic)
        resultdict['code'] = 0
        resultdict['msg'] = "任务列表"
        resultdict['count'] = total
        resultdict['data'] = data
        return JsonResponse(resultdict)

################################
# 任务列表
################################
TASK_STATUS = {
        '0': '审批中',
        '1': '待执行',
        '2': '执行中',
        '3': '已暂停',
        '4': '已完成',
        '5': '已结束',}

@login_required
@csrf_protect
def task_list(request):
    user = request.user
    error = ''

    resultdict = dict()

    page = request.POST.get('page')
    rows = request.POST.get('limit')

    name = request.POST.get('name')
    if not name:
        name = ''

    key = request.POST.get('key')
    if not key:
        key = ''

    tasktype = request.POST.get('type')
    if not type:
        tasktype = ['安全扫描', '扫描同步']
    else:
        tasktype = [tasktype]

    taskstatus = request.POST.get('status')
    if not taskstatus:
        if user.is_superuser:
            taskstatus = [str(i) for i in range(1, 6)]
        else:
            taskstatus = [str(i) for i in range(0, 6)]
    else:
        taskstatus = [taskstatus]

    if user.is_superuser:
        task_list = models.Task.objects.filter(
            task_name__icontains=name,
            task_type__icontains=key,
            task_type__in=tasktype,
            task_status__in=taskstatus
        ).order_by('task_status', '-task_endtime')
    else:
        task_list = models.Task.objects.filter(
            task_user=user,
            task_name__icontains=name,
            task_type__icontains=key,
            task_type__in=tasktype,
            task_status__in=taskstatus
        ).order_by('task_status', '-task_endtime')
    # 统计总数
    total = task_list.count()
    # 分页显示
    task_list = paging(task_list, rows, page)
    data = list()
    for item in task_list:
        dic = dict()
        dic['task_id'] = escape(item.task_id)
        dic['task_name'] = escape(item.task_name)
        dic['task_type'] = escape(item.task_type)
        dic['task_target'] = escape(item.task_target)
        dic['task_status'] = escape(TASK_STATUS[item.task_status])
        dic['task_starttime'] = escape(item.task_starttime)
        dic['task_scanner'] = escape(item.task_scanner)
        dic['task_user'] = escape(item.task_user)
        data.append(dic)
    resultdict['code'] = '0'
    resultdict['msg'] = '任务列表'
    resultdict['count'] = total
    resultdict['data'] = data
    return JsonResponse(resultdict)

################################
# 任务详情
################################
@login_required
def taskdetails(request, task_id):
    user = request.user
    print('任务ID:', task_id)
    if user.is_superuser:
        task = get_object_or_404(models.Task, task_id=task_id)
    else:
        task = get_object_or_404(models.Task, task_user=user, task_id=task_id)
    return render(request, 'TaskManage/taskdetails.html', dict(task=task))



################################
# 启动
################################
@login_required
def task_action(request, task_id, action):
    user = request.user
    error = ''

    if user.is_superuser:
        task =models.Task.objects.exclude(task_status=0).filter(task_status__lt=4,task_id = task_id).first()
    else:
        task = user.task_for_user.exclude(task_status=0).filter(task_status__lt=4,request_status='1',task_id = task_id).first()
    if task:
        if task.task_scanner.scanner_type == 'Nessus':
            res = sys_action(request,task,action)
            if res:
                error = '执行成功'
            else:
                error = '操作失误，请联系管理员'
        elif task.task_scanner.scanner_type == 'AWVS':
            res = web_action(request, task, action)
            if res:
                error = '执行成功'
            else:
                error = '操作失误，请联系管理员'
        elif task.task_scanner.scanner_type == 'MobSF':
            error = '暂未提供该类任务'
    else:
        error = '请不要随意更改参数哦，会被记住的'
    return JsonResponse(dict(error=error))


################################
# 任务审批
###############################
@login_required
@csrf_protect
def taskrequestaction(request, task_id, action):
    user = request.user
    error = ''

    if user.is_superuser:
        task = get_object_or_404(models.Task, task_id=task_id)
        if action == 'access':
            task.task_status = '1'
            task.request_status = '1'
            task.request_note = '已验证'
            task.action_user = user
        elif action == 'deny':
            task.task_status = '5'
            task.request_status = '2'
            task.action_user = user
        task.save()
        error = '审批完成'
    else:
        error = '权限错误'
    return JsonResponse(dict(error=error))

################################
# 扫描同步
###############################
@login_required
@csrf_protect
def task_sync(request):
    user = request.user
    error = ''
    if user.is_superuser:
        if request.method == 'POST':
            form = forms.TaskSyncForm(request.POST, request.FILES)
            if form.is_valid():
                task_scanner = form.cleaned_data['task_scanner']
                if task_scanner.scanner_type == 'Nessus':
                    try:
                        num_id = models.Task.objects.latest('id').id
                    except:
                        num_id = 0
                    num_id += 1
                    task_id = str('s') + time.strftime('%Y%m%d', time.localtime(time.time())) + str(num_id)
                    task_name = form.cleaned_data['task_name']
                    task_type = '扫描同步'
                    scan_type = ''

                    scan_id = form.cleaned_data['scan_id']
                    task_info = form.cleaned_data['task_info']

                    models.Task.objects.get_or_create(
                        task_id=task_id,
                        task_name=task_name,
                        task_type=task_type,
                        task_scanner=task_scanner,
                        scan_id=scan_id,
                        task_status='1',
                        task_user=user,
                        task_targetinfo=task_info
                    )
                    error = '创建成功'
                else:
                    error = '扫描节点不支持导入'
        else:
            form = forms.TaskSyncForm()
        return render(request, 'formedit.html', {'form': form, 'post_url': 'tasksync', 'error': error})
    else:
        error = '权限错误'
    return render(request, 'error.html', dict(error=error))