#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'

from django.shortcuts import render


################################
# 漏洞管理
################################
def vuln_view(request):
    return render(request, 'VulnManage/vulnlist.html')