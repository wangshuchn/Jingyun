#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : urls.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/19 15:21
    *  @Software : PyCharm
    ***********************************
"""
from django.urls import path
from . import views


urlpatterns = [
    path('user/', views.vuln_view, name='vulnview'),
]